package id.ac.tazkia.umum.aplikasiumum.controller.masterdata;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class RoomController {

    @GetMapping("/masters/room")
    public String listRoom(){

        return "masters/room/list";
    }

}
