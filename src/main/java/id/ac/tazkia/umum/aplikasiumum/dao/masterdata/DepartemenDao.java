package id.ac.tazkia.umum.aplikasiumum.dao.masterdata;

import id.ac.tazkia.umum.aplikasiumum.entity.StatusRecord;
import id.ac.tazkia.umum.aplikasiumum.entity.masterdata.Departemen;
import id.ac.tazkia.umum.aplikasiumum.entity.masterdata.Instansi;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface DepartemenDao extends PagingAndSortingRepository<Departemen, String> {

        Page<Departemen> findByStatusOrderByKodeDepartemen(StatusRecord statusRecord, Pageable page);

        Page<Departemen> findByStatusAndKodeDepartemenContainingOrNamaDepartemenContainingOrderByKodeDepartemen(StatusRecord statusRecord, String kode, String nama, Pageable page);

        List<Departemen> findByStatusAndStatusAktifOrderByKodeDepartemen(StatusRecord statusRecord, StatusRecord statusAktif);

        Integer countByStatusAndInstansi(StatusRecord statusRecord, Instansi instansi);


}
