package id.ac.tazkia.umum.aplikasiumum.controller.masterdata;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class EmployeeController {

    @GetMapping("/masters/employee")
    public String listEmployee(){

        return "masters/employee/list";
    }

}
