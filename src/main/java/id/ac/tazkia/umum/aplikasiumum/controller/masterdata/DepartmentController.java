package id.ac.tazkia.umum.aplikasiumum.controller.masterdata;


import id.ac.tazkia.umum.aplikasiumum.dao.masterdata.DepartemenDao;
import id.ac.tazkia.umum.aplikasiumum.entity.StatusRecord;
import id.ac.tazkia.umum.aplikasiumum.entity.masterdata.Departemen;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class DepartmentController {

    @Autowired
    private DepartemenDao departemenDao;

    @GetMapping("/masters/department")
    public String listDepartment(Model model,
                                 @RequestParam(required = false)String search,
                                 @PageableDefault(size = 10)Pageable page){

        if (StringUtils.hasText(search)) {
            model.addAttribute("search", search);
            model.addAttribute("listDepartemen", departemenDao.findByStatusAndKodeDepartemenContainingOrNamaDepartemenContainingOrderByKodeDepartemen(StatusRecord.AKTIF, search, search, page));
        }else{
            model.addAttribute("listDepartemen", departemenDao.findByStatusOrderByKodeDepartemen(StatusRecord.AKTIF, page));
        }


        return "masters/department/list";
    }
    
    @GetMapping("/masters/departement")
    public String newDepartemen(Model model){

        model.addAttribute("departemen",new Departemen());

        return "masters/departemen/form";
    }
}
