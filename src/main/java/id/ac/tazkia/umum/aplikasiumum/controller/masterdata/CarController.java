package id.ac.tazkia.umum.aplikasiumum.controller.masterdata;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class CarController {

    @GetMapping("/masters/car")
    public String listCar(){

        return "masters/car/list";
    }

}
