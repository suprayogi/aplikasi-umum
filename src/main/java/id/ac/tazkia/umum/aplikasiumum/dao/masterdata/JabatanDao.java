package id.ac.tazkia.umum.aplikasiumum.dao.masterdata;


import id.ac.tazkia.umum.aplikasiumum.entity.StatusRecord;
import id.ac.tazkia.umum.aplikasiumum.entity.masterdata.Jabatan;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface JabatanDao extends PagingAndSortingRepository<Jabatan, String> {

    Page<Jabatan> findByStatusOrderByKodeJabatan(StatusRecord statusRecord, Pageable page);

    Page<Jabatan> findByStatusAndKodeJabatanContainingIgnoreCaseOrNamaJabatanContainingIgnoreCaseOrderByKodeJabatan(StatusRecord statusRecord, String kodeJabatan, String namaJabatan, Pageable page);

    List<Jabatan> findByStatusOrderByKodeJabatan(StatusRecord statusRecord);

    List<Jabatan> findByStatusAndStatusAktifOrderByKodeJabatan(StatusRecord statusRecord, StatusRecord statusAktif);


}
