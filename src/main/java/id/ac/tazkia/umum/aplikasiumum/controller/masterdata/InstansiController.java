package id.ac.tazkia.umum.aplikasiumum.controller.masterdata;

import id.ac.tazkia.umum.aplikasiumum.dao.masterdata.DepartemenDao;
import id.ac.tazkia.umum.aplikasiumum.dao.masterdata.InstansiDao;
import id.ac.tazkia.umum.aplikasiumum.entity.masterdata.Departemen;
import id.ac.tazkia.umum.aplikasiumum.entity.masterdata.Instansi;
import id.ac.tazkia.umum.aplikasiumum.entity.StatusRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.persistence.criteria.CriteriaBuilder;
import javax.validation.Valid;
import java.math.BigDecimal;

@Controller
public class InstansiController {

    @Autowired
    private InstansiDao instansiDao;

    @Autowired
    private DepartemenDao departemenDao;

    @GetMapping("/masters/instansi")
    public String listInstansi(Model model,
                               @RequestParam(required = false)String search,
                               @PageableDefault(size = 10) Pageable page){


        if (StringUtils.hasText(search)) {
            model.addAttribute("search", search);
            model.addAttribute("listInstansi", instansiDao.findByStatusAndNamaContainingOrderByNama(StatusRecord.AKTIF, search, page));
        }else{
            model.addAttribute("listInstansi", instansiDao.findByStatusOrderByNama(StatusRecord.AKTIF, page));
        }

        return "masters/instansi/list";

    }

    @GetMapping("/masters/instansi/new")
    public String newInstansi(Model model){

        model.addAttribute("instansi", new Instansi());

        return "masters/instansi/form";

    }

    @GetMapping("/masters/instansi/edit")
    public String editInstansi(Model model,
                               @RequestParam(required = false) String instansi){

        model.addAttribute("instansi", instansiDao.findById(instansi));

        return "masters/instansi/form";

    }

    @PostMapping("/masters/instansi/save")
    public String saveInstansi(@ModelAttribute @Valid Instansi instansi,
                               BindingResult errors,
                               RedirectAttributes attributes){

        if(errors.hasErrors()){
            return "masters/instansi/form";
        }

        instansi.setStatus(StatusRecord.AKTIF);
        instansiDao.save(instansi);

        attributes.addFlashAttribute("success", "Save Data Berhasil");
        return "redirect:../instansi";

    }

    @PostMapping("/masters/instansi/hapus")
    public String departemenHapus(@RequestParam(required = false) Instansi instansi,
                                  RedirectAttributes attributes){

        Instansi instansi1 = instansiDao.findById(instansi.getId()).get();

        Integer departemen = departemenDao.countByStatusAndInstansi(StatusRecord.AKTIF, instansi);

        if(departemen > 0){
            attributes.addFlashAttribute("gagal", "Save Data Gagal");
            return "redirect:../instansi";
        }

        instansi1.setStatus(StatusRecord.HAPUS);
        instansiDao.save(instansi1);

        attributes.addFlashAttribute("delete", "Save Data Berhasil");
        return "redirect:../instansi";

    }

}
