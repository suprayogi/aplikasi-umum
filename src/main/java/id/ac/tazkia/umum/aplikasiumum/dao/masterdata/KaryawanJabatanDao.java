package id.ac.tazkia.umum.aplikasiumum.dao.masterdata;

import id.ac.tazkia.umum.aplikasiumum.entity.StatusRecord;
import id.ac.tazkia.umum.aplikasiumum.entity.masterdata.Departemen;
import id.ac.tazkia.umum.aplikasiumum.entity.masterdata.Karyawan;
import id.ac.tazkia.umum.aplikasiumum.entity.masterdata.KaryawanJabatan;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import java.time.LocalDate;
import java.util.List;

public interface KaryawanJabatanDao extends PagingAndSortingRepository<KaryawanJabatan, String> {

    Page<KaryawanJabatan> findByStatusAndKaryawanIdOrderByMulaiBerlaku(StatusRecord statusRecord, String karyawan, Pageable page);

    List<KaryawanJabatan> findByStatusAndStatusAktifAndKaryawanAndMulaiBerlakuBefore(StatusRecord statusRecord, StatusRecord statusAktif, Karyawan karyawan, LocalDate tanggal);

    KaryawanJabatan findByStatusAndStatusAktifAndKaryawanAndJabatanDepartemen(StatusRecord statusRecord, StatusRecord statusAktif, Karyawan karyawan, Departemen departemen);

    @Query("select kd.jabatan.departemen.id from KaryawanJabatan kd where kd.status = :status and statusAktif='AKTIF' and kd.karyawan = :karyawan and kd.mulaiBerlaku <= :tanggal")
    List<String> cariId(@Param("status") StatusRecord statusRecord, @Param("karyawan") Karyawan karyawan, @Param("tanggal") LocalDate tanggal);

    @Query(value = "\n" +
            "SELECT * FROM\n" +
            "(SELECT dp.id AS id_departemen FROM karyawan_jabatan AS kj INNER JOIN jabatan AS ja ON kj.id_jabatan=ja.id INNER JOIN \n" +
            "departemen AS dp ON ja.id_departemen=dp.id WHERE kj.status='AKTIF' AND kj.status_aktif='AKTIF' AND kj.id_karyawan=?1 AND kj.mulai_berlaku <= DATE(NOW())\n" +
            "UNION\n" +
            "SELECT da.id AS id_departemen FROM karyawan_jabatan AS kj INNER JOIN setting_approval_pengajuan_dana AS sa ON kj.id_jabatan = sa.id_jabatan_approve\n" +
            "INNER JOIN jabatan AS ja ON sa.id_jabatan=ja.id\n" +
            "INNER JOIN departemen AS da ON ja.id_departemen = da.id\n" +
            "WHERE kj.status='AKTIF' AND kj.status_aktif='AKTIF' AND kj.id_karyawan= ?1 AND kj.mulai_berlaku <= DATE(NOW()) GROUP BY da.id) a GROUP BY id_departemen\n", nativeQuery = true)
    List<String> cariIdDepartemen(String idKaryawan);


    @Query("select kd.jabatan.id from KaryawanJabatan kd where kd.status = :status and statusAktif='AKTIF' and kd.karyawan = :karyawan and kd.mulaiBerlaku <= :tanggal")
    List<String> cariIdJabatan(@Param("status") StatusRecord statusRecord, @Param("karyawan") Karyawan karyawan, @Param("tanggal") LocalDate tanggal);

    KaryawanJabatan findByStatusAndStatusAktifAndJabatanIdAndMulaiBerlakuBefore(StatusRecord statusRecord, StatusRecord statusAktif, String jabatan, LocalDate mulaiBerlaku);

    @Query("select kd.jabatan.departemen.instansi.id from KaryawanJabatan kd where kd.status = :status and statusAktif='AKTIF' and kd.karyawan = :karyawan and kd.mulaiBerlaku <= :tanggal")
    List<String> cariIdInstansi(@Param("status") StatusRecord statusRecord, @Param("karyawan") Karyawan karyawan, @Param("tanggal") LocalDate tanggal);

    @Query(value = "select count(id) as jml from\n" +
            "(select a.id from karyawan_jabatan as a inner join jabatan as b on a.id_jabatan = b.id inner join departemen as c on b.id_departemen=c.id\n" +
            "inner join instansi as d on c.id_instansi=d.id where id_karyawan=?1 group by id_instansi)aa", nativeQuery = true)
    Integer jmlInstansi(String idKaryawan);



}
