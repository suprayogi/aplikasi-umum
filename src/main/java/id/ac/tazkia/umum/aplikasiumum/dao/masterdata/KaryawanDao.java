package id.ac.tazkia.umum.aplikasiumum.dao.masterdata;

import id.ac.tazkia.umum.aplikasiumum.entity.StatusRecord;
import id.ac.tazkia.umum.aplikasiumum.entity.config.User;
import id.ac.tazkia.umum.aplikasiumum.entity.masterdata.Karyawan;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface KaryawanDao extends PagingAndSortingRepository<Karyawan, String> {

    Page<Karyawan> findByStatusOrderByNamaKaryawan(StatusRecord statusRecord, Pageable page);

    Page<Karyawan> findByStatusAndNikContainingIgnoreCaseOrNamaKaryawanContainingIgnoreCaseOrderByNamaKaryawan(StatusRecord statusRecord, String nik, String nama, Pageable page);

    List<Karyawan> findByStatusOrderByNamaKaryawanAsc(StatusRecord statusRecord);

    Karyawan findByUser(User user);

}
